const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin-webpack4');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PreloadPlugin = require('@vue/preload-webpack-plugin');
const HtmlPwaPlugin = require('@vue/cli-plugin-pwa/lib/HtmlPwaPlugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: 'development',
    context: process.cwd(),
    node: {
        setImmediate: false,
        process: 'mock',
        dgram: 'empty',
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty',
    },
    output: {
        path: './dist',
        filename: 'js/[name].js',
        publicPath: '/',
        chunkFilename: 'js/[name].js',
    },
    resolve: {
        alias: {
            '@': path.resolve('./src'),
            vue$: 'vue/dist/vue.runtime.esm.js',
        },
        extensions: ['.tsx', '.ts', '.mjs', '.js', '.jsx', '.vue', '.json', '.wasm', '.md'],
        modules: ['node_modules', path.join(process.cwd(), 'node_modules'), path.join(process.cwd(), 'node_modules/@vue/cli-service/node_modules')],
        plugins: [
            /* config.resolve.plugin('pnp') */
            // {},
        ],
    },
    resolveLoader: {
        modules: [
            path.join(process.cwd(), 'node_modules/@vue/cli-plugin-typescript/node_modules'),
            path.join(process.cwd(), 'node_modules/@vue/cli-plugin-babel/node_modules'),
            'node_modules',
            path.join(process.cwd(), 'node_modules'),
            path.join(process.cwd(), 'node_modules/@vue/cli-service/node_modules'),
        ],
        plugins: [
            /* config.resolve.plugin('pnp-loaders') */
            // {},
        ],
    },
    module: {
        noParse: /^(vue|vue-router|vuex|vuex-router-sync)$/,
        rules: [
            /* config.module.rule('vue') */
            {
                test: /\.vue$/,
                use: [
                    /* config.module.rule('vue').use('cache-loader') */
                    {
                        loader: 'cache-loader',
                        options: {
                            cacheDirectory: path.join(process.cwd(), 'node_modules/.cache/vue-loader'),
                            cacheIdentifier: '142ea8a2',
                        },
                    },
                    /* config.module.rule('vue').use('vue-loader') */
                    {
                        loader: 'vue-loader',
                        options: {
                            compilerOptions: {
                                whitespace: 'condense',
                            },
                            cacheDirectory: path.join(process.cwd(), 'node_modules/.cache/vue-loader'),
                            cacheIdentifier: '142ea8a2',
                        },
                    },
                ],
            },
            /* config.module.rule('images') */
            {
                test: /\.(png|jpe?g|gif|webp)(\?.*)?$/,
                use: [
                    /* config.module.rule('images').use('url-loader') */
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 4096,
                            fallback: {
                                loader: 'file-loader',
                                options: {
                                    name: 'img/[name].[hash:8].[ext]',
                                },
                            },
                        },
                    },
                ],
            },
            /* config.module.rule('svg') */
            {
                test: /\.(svg)(\?.*)?$/,
                use: [
                    /* config.module.rule('svg').use('file-loader') */
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'img/[name].[hash:8].[ext]',
                        },
                    },
                ],
            },
            /* config.module.rule('media') */
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
                use: [
                    /* config.module.rule('media').use('url-loader') */
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 4096,
                            fallback: {
                                loader: 'file-loader',
                                options: {
                                    name: 'media/[name].[hash:8].[ext]',
                                },
                            },
                        },
                    },
                ],
            },
            /* config.module.rule('fonts') */
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
                use: [
                    /* config.module.rule('fonts').use('url-loader') */
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 4096,
                            fallback: {
                                loader: 'file-loader',
                                options: {
                                    name: 'fonts/[name].[hash:8].[ext]',
                                },
                            },
                        },
                    },
                ],
            },
            /* config.module.rule('pug') */
            {
                test: /\.pug$/,
                oneOf: [
                    /* config.module.rule('pug').oneOf('pug-vue') */
                    {
                        resourceQuery: /vue/,
                        use: [
                            /* config.module.rule('pug').oneOf('pug-vue').use('pug-plain-loader') */
                            {
                                loader: 'pug-plain-loader',
                            },
                        ],
                    },
                    /* config.module.rule('pug').oneOf('pug-template') */
                    {
                        use: [
                            /* config.module.rule('pug').oneOf('pug-template').use('raw') */
                            {
                                loader: 'raw-loader',
                            },
                            /* config.module.rule('pug').oneOf('pug-template').use('pug-plain-loader') */
                            {
                                loader: 'pug-plain-loader',
                            },
                        ],
                    },
                ],
            },
            /* config.module.rule('css') */
            {
                test: /\.css$/,
                oneOf: [
                    /* config.module.rule('css').oneOf('vue-modules') */
                    {
                        resourceQuery: /module/,
                        use: [
                            /* config.module.rule('css').oneOf('vue-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('css').oneOf('vue-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('css').oneOf('vue-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('css').oneOf('vue') */
                    {
                        resourceQuery: /\?vue/,
                        use: [
                            /* config.module.rule('css').oneOf('vue').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('css').oneOf('vue').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('css').oneOf('vue').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('css').oneOf('normal-modules') */
                    {
                        test: /\.module\.\w+$/,
                        use: [
                            /* config.module.rule('css').oneOf('normal-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('css').oneOf('normal-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('css').oneOf('normal-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('css').oneOf('normal') */
                    {
                        use: [
                            /* config.module.rule('css').oneOf('normal').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('css').oneOf('normal').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('css').oneOf('normal').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                ],
            },
            /* config.module.rule('postcss') */
            {
                test: /\.p(ost)?css$/,
                oneOf: [
                    /* config.module.rule('postcss').oneOf('vue-modules') */
                    {
                        resourceQuery: /module/,
                        use: [
                            /* config.module.rule('postcss').oneOf('vue-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('postcss').oneOf('vue-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('postcss').oneOf('vue-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('postcss').oneOf('vue') */
                    {
                        resourceQuery: /\?vue/,
                        use: [
                            /* config.module.rule('postcss').oneOf('vue').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('postcss').oneOf('vue').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('postcss').oneOf('vue').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('postcss').oneOf('normal-modules') */
                    {
                        test: /\.module\.\w+$/,
                        use: [
                            /* config.module.rule('postcss').oneOf('normal-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('postcss').oneOf('normal-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('postcss').oneOf('normal-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('postcss').oneOf('normal') */
                    {
                        use: [
                            /* config.module.rule('postcss').oneOf('normal').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('postcss').oneOf('normal').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('postcss').oneOf('normal').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                ],
            },
            /* config.module.rule('scss') */
            {
                test: /\.scss$/,
                oneOf: [
                    /* config.module.rule('scss').oneOf('vue-modules') */
                    {
                        resourceQuery: /module/,
                        use: [
                            /* config.module.rule('scss').oneOf('vue-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('vue-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('scss').oneOf('vue-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('vue-modules').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('scss').oneOf('vue') */
                    {
                        resourceQuery: /\?vue/,
                        use: [
                            /* config.module.rule('scss').oneOf('vue').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('vue').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('scss').oneOf('vue').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('vue').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('scss').oneOf('normal-modules') */
                    {
                        test: /\.module\.\w+$/,
                        use: [
                            /* config.module.rule('scss').oneOf('normal-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('normal-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('scss').oneOf('normal-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('normal-modules').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('scss').oneOf('normal') */
                    {
                        use: [
                            /* config.module.rule('scss').oneOf('normal').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('normal').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('scss').oneOf('normal').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('scss').oneOf('normal').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                ],
            },
            /* config.module.rule('sass') */
            {
                test: /\.sass$/,
                oneOf: [
                    /* config.module.rule('sass').oneOf('vue-modules') */
                    {
                        resourceQuery: /module/,
                        use: [
                            /* config.module.rule('sass').oneOf('vue-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('vue-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('sass').oneOf('vue-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('vue-modules').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                    sassOptions: {
                                        indentedSyntax: true,
                                    },
                                },
                            },
                        ],
                    },
                    /* config.module.rule('sass').oneOf('vue') */
                    {
                        resourceQuery: /\?vue/,
                        use: [
                            /* config.module.rule('sass').oneOf('vue').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('vue').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('sass').oneOf('vue').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('vue').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                    sassOptions: {
                                        indentedSyntax: true,
                                    },
                                },
                            },
                        ],
                    },
                    /* config.module.rule('sass').oneOf('normal-modules') */
                    {
                        test: /\.module\.\w+$/,
                        use: [
                            /* config.module.rule('sass').oneOf('normal-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('normal-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('sass').oneOf('normal-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('normal-modules').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                    sassOptions: {
                                        indentedSyntax: true,
                                    },
                                },
                            },
                        ],
                    },
                    /* config.module.rule('sass').oneOf('normal') */
                    {
                        use: [
                            /* config.module.rule('sass').oneOf('normal').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('normal').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('sass').oneOf('normal').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('sass').oneOf('normal').use('sass-loader') */
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: false,
                                    sassOptions: {
                                        indentedSyntax: true,
                                    },
                                },
                            },
                        ],
                    },
                ],
            },
            /* config.module.rule('less') */
            {
                test: /\.less$/,
                oneOf: [
                    /* config.module.rule('less').oneOf('vue-modules') */
                    {
                        resourceQuery: /module/,
                        use: [
                            /* config.module.rule('less').oneOf('vue-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('vue-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('less').oneOf('vue-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('vue-modules').use('less-loader') */
                            {
                                loader: 'less-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('less').oneOf('vue') */
                    {
                        resourceQuery: /\?vue/,
                        use: [
                            /* config.module.rule('less').oneOf('vue').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('vue').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('less').oneOf('vue').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('vue').use('less-loader') */
                            {
                                loader: 'less-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('less').oneOf('normal-modules') */
                    {
                        test: /\.module\.\w+$/,
                        use: [
                            /* config.module.rule('less').oneOf('normal-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('normal-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('less').oneOf('normal-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('normal-modules').use('less-loader') */
                            {
                                loader: 'less-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                    /* config.module.rule('less').oneOf('normal') */
                    {
                        use: [
                            /* config.module.rule('less').oneOf('normal').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('normal').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('less').oneOf('normal').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('less').oneOf('normal').use('less-loader') */
                            {
                                loader: 'less-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                        ],
                    },
                ],
            },
            /* config.module.rule('stylus') */
            {
                test: /\.styl(us)?$/,
                oneOf: [
                    /* config.module.rule('stylus').oneOf('vue-modules') */
                    {
                        resourceQuery: /module/,
                        use: [
                            /* config.module.rule('stylus').oneOf('vue-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('vue-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('stylus').oneOf('vue-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('vue-modules').use('stylus-loader') */
                            {
                                loader: 'stylus-loader',
                                options: {
                                    sourceMap: false,
                                    preferPathResolver: 'webpack',
                                },
                            },
                        ],
                    },
                    /* config.module.rule('stylus').oneOf('vue') */
                    {
                        resourceQuery: /\?vue/,
                        use: [
                            /* config.module.rule('stylus').oneOf('vue').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('vue').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('vue').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('vue').use('stylus-loader') */
                            {
                                loader: 'stylus-loader',
                                options: {
                                    sourceMap: false,
                                    preferPathResolver: 'webpack',
                                },
                            },
                        ],
                    },
                    /* config.module.rule('stylus').oneOf('normal-modules') */
                    {
                        test: /\.module\.\w+$/,
                        use: [
                            /* config.module.rule('stylus').oneOf('normal-modules').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('normal-modules').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                    modules: {
                                        localIdentName: '[name]_[local]_[hash:base64:5]',
                                    },
                                },
                            },
                            /* config.module.rule('stylus').oneOf('normal-modules').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('normal-modules').use('stylus-loader') */
                            {
                                loader: 'stylus-loader',
                                options: {
                                    sourceMap: false,
                                    preferPathResolver: 'webpack',
                                },
                            },
                        ],
                    },
                    /* config.module.rule('stylus').oneOf('normal') */
                    {
                        use: [
                            /* config.module.rule('stylus').oneOf('normal').use('vue-style-loader') */
                            {
                                loader: 'vue-style-loader',
                                options: {
                                    sourceMap: false,
                                    shadowMode: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('normal').use('css-loader') */
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: false,
                                    importLoaders: 2,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('normal').use('postcss-loader') */
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: false,
                                },
                            },
                            /* config.module.rule('stylus').oneOf('normal').use('stylus-loader') */
                            {
                                loader: 'stylus-loader',
                                options: {
                                    sourceMap: false,
                                    preferPathResolver: 'webpack',
                                },
                            },
                        ],
                    },
                ],
            },
            /* config.module.rule('js') */
            {
                test: /\.m?jsx?$/,
                // exclude: [
                //     function() {
                //         /* omitted long function */
                //     },
                // ],
                use: [
                    /* config.module.rule('js').use('cache-loader') */
                    {
                        loader: 'cache-loader',
                        options: {
                            cacheDirectory: path.join(process.cwd(), '/node_modules/.cache/babel-loader'),
                            cacheIdentifier: '6624edfc',
                        },
                    },
                    /* config.module.rule('js').use('babel-loader') */
                    {
                        loader: 'babel-loader',
                    },
                ],
            },
            /* config.module.rule('ts') */
            {
                test: /\.ts$/,
                use: [
                    /* config.module.rule('ts').use('cache-loader') */
                    {
                        loader: 'cache-loader',
                        options: {
                            cacheDirectory: path.join(process.cwd(), '/node_modules/.cache/ts-loader'),
                            cacheIdentifier: 'afefe0b4',
                        },
                    },
                    /* config.module.rule('ts').use('babel-loader') */
                    {
                        loader: 'babel-loader',
                    },
                    /* config.module.rule('ts').use('ts-loader') */
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            appendTsSuffixTo: ['/.vue$'],
                            happyPackMode: false,
                        },
                    },
                ],
            },
            /* config.module.rule('tsx') */
            {
                test: /\.tsx$/,
                use: [
                    /* config.module.rule('tsx').use('cache-loader') */
                    {
                        loader: 'cache-loader',
                        options: {
                            cacheDirectory: path.join(process.cwd(), '/node_modules/.cache/ts-loader'),
                            cacheIdentifier: 'afefe0b4',
                        },
                    },
                    /* config.module.rule('tsx').use('babel-loader') */
                    {
                        loader: 'babel-loader',
                    },
                    /* config.module.rule('tsx').use('ts-loader') */
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            happyPackMode: false,
                            appendTsxSuffixTo: ['/.vue$'],
                        },
                    },
                ],
            },
            /* config.module.rule('docs') */
            {
                resourceQuery: /blockType=docs/,
                use: [
                    /* config.module.rule('docs').use('docs-ignore-loader') */
                    {
                        loader: 'vue-cli-plugin-styleguidist',
                    },
                ],
            },
        ],
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendors: {
                    name: 'chunk-vendors',
                    test: /[//]node_modules[//]/,
                    priority: -10,
                    chunks: 'initial',
                },
                common: {
                    name: 'chunk-common',
                    minChunks: 2,
                    priority: -20,
                    chunks: 'initial',
                    reuseExistingChunk: true,
                },
            },
        },
        minimizer: [
            /* config.optimization.minimizer('terser') */
            new TerserPlugin({
                terserOptions: {
                    compress: {
                        arrows: false,
                        collapse_vars: false,
                        comparisons: false,
                        computed_props: false,
                        hoist_funs: false,
                        hoist_props: false,
                        hoist_vars: false,
                        inline: false,
                        loops: false,
                        negate_iife: false,
                        properties: false,
                        reduce_funcs: false,
                        reduce_vars: false,
                        switches: false,
                        toplevel: false,
                        typeofs: false,
                        booleans: true,
                        if_return: true,
                        sequences: true,
                        unused: true,
                        conditionals: true,
                        dead_code: true,
                        evaluate: true,
                    },
                    mangle: {
                        safari10: true,
                    },
                },
                sourceMap: true,
                cache: true,
                parallel: true,
                extractComments: false,
            }),
        ],
    },
    plugins: [
        /* config.plugin('vue-loader') */
        new VueLoaderPlugin(),
        /* config.plugin('define') */
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"development"',
                VUE_APP_API_HOST: '"http://localhost:3000/graphql"',
                BASE_URL: '"/"',
            },
        }),
        /* config.plugin('case-sensitive-paths') */
        new CaseSensitivePathsPlugin(),
        /* config.plugin('friendly-errors') */
        new FriendlyErrorsWebpackPlugin({
            additionalTransformers: [
                function() {
                    /* omitted long function */
                },
            ],
            additionalFormatters: [
                function() {
                    /* omitted long function */
                },
            ],
        }),
        /* config.plugin('html') */
        new HtmlWebpackPlugin({
            title: 'dashboard',
            templateParameters: function() {
                /* omitted long function */
            },
            template: path.resolve('./public/index.html'),
        }),
        /* config.plugin('pwa') */
        new HtmlPwaPlugin({
            name: 'dashboard',
            manifestOptions: {
                name: 'dashboard',
                short_name: 'dashboard',
                icons: [
                    {
                        src: './img/icons/android-chrome-192x192.png',
                        sizes: '192x192',
                        type: 'image/png',
                    },
                    {
                        src: './img/icons/android-chrome-512x512.png',
                        sizes: '512x512',
                        type: 'image/png',
                    },
                ],
                start_url: './index.html',
                display: 'standalone',
                background_color: '#000000',
                theme_color: '#4DBA87',
            },
        }),
        // /* config.plugin('preload') */
        new PreloadPlugin({
            rel: 'preload',
            include: 'initial',
            fileBlacklist: [/\.map$/, /hot-update\.js$/],
        }),
        // /* config.plugin('prefetch') */
        new PreloadPlugin({
            rel: 'prefetch',
            include: 'asyncChunks',
        }),
        // /* config.plugin('copy') */
        new CopyPlugin([
            {
                from: path.resolve('./public'),
                to: path.resolve('./dist'),
                toType: 'dir',
                ignore: [
                    '.DS_Store',
                    {
                        glob: 'index.html',
                        matchBase: false,
                    },
                ],
            },
        ]),
        // /* config.plugin('fork-ts-checker') */
        new ForkTsCheckerWebpackPlugin({
            vue: {
                enabled: true,
                compiler: 'vue-template-compiler',
            },
            tslint: true,
            formatter: 'codeframe',
            checkSyntacticErrors: false,
        }),
    ],
    entry: {
        app: ['./src/main.ts'],
    },
};
