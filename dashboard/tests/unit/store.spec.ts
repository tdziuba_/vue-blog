import store from '@/store';

describe('store', () => {
    it('should have correct initial state', () => {
        expect(store.state).toEqual({});
    });
});
