const path = require('path');

module.exports = {
    title: 'Dashboard Style Guide',

    jsxInComponents: true,
    jssThemedEditor: false,
    defaultExample: true,
    progressBar: true,
    pagePerSection: true,
    styles: {
        Editor: {
            root: {
                isolate: false,
            },
        },
    },
    require: [path.join(__dirname, 'src/scss/app.scss')],

    // components: 'src/components/**/[A-Z]*.vue',
    components: 'src/components/Alert/**/[A-Z]*.vue',
    ignore: ['src/components/Tags/TagsList/**/[A-Z]*.vue'],
    defaultExample: true,
    styleguideDir: 'dist/styleguide',
    sections: [
        {
            name: 'Common components',
            components: ['src/components/Alert**/[A-Z]*.vue', 'src/components/Loader**/[A-Z]*.vue'],
        },
        {
            name: 'Buttons',
            components: ['src/components/Buttons/**/**/[A-Z]*.vue'],
        },
        {
            name: 'Form components',
            components: ['src/components/Form/**/**/[A-Z]*.vue'],
        },
        {
            name: 'Tags components',
            components: ['src/components/Tags/Tag/**/[A-Z]*.vue', 'src/components/Tags/TagForm/**/[A-Z]*.vue'],
        },
        // {
        //     name: 'Containers',
        //     components: 'src/containers/**/[A-Z]*.vue',
        // },
    ],

    exampleMode: 'expand',
    usageMode: 'expand',
};
