module.exports = {
    moduleFileExtensions: ['js', 'vue', 'ts', 'tsx'],
    transform: {
        // '.*\\.(vue)$': '<rootDir>/node_modules/jest-vue-preprocessor',
        '.*\\.(vue)$': 'vue-jest',
        '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
        '^.+\\.tsx?$': 'ts-jest',
        '.*': '<rootDir>/node_modules/babel-jest',
    },
    roots: ['<rootDir>/src/', '<rootDir>/tests'],
    testMatch: ['<rootDir>/src/components/**/*.(spec).{ts,tsx}', '<rootDir>/tests/unit/**/**/*.(spec).{ts,tsx}', '**/?(*.)+(spec).[tj]s?(x)'],
    transformIgnorePatterns: ['/node_modules/'],
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1',
    },
    snapshotSerializers: ['jest-serializer-vue'],
    testURL: 'http://localhost/',
    watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
    globals: {
        'ts-jest': {
            babelConfig: true,
            tsconfig: true,
        },
    },
    // coverage
    collectCoverageFrom: [
        'src/**/*.{ts, tsx, vue}',
        'src/components/*.{ts, tsx, vue}',
        'src/views/*.{ts, tsx, vue}',
        '!**/?(*.)+(spec).[tj]s?(x)',
        '!**/node_modules/**',
        '!**/vendor/**',
        '!src/main.ts',
        '!src/registerServiceWorker.ts',
    ],
    coverageDirectory: './dist/coverage',
    coveragePathIgnorePatterns: ['/node_modules/', '<rootDir>/dist/', '<rootDir>/src/plugins/'],
    coverageReporters: ['json', 'lcov', 'text', 'clover', 'html'],
};
