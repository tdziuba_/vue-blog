# dashboard

Before it starts (for development), you'll need to run api first. Go to ../api and run `yarn start:dev`. It will serve graphql api backend to be connected with dashboard.

If you plan to develop single components you can use just styleguide: `yarn styleguide`;

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Run your end-to-end tests

```
yarn run test:e2e
```

### Run your unit tests

```
yarn run test:unit
```

### Run styleguidist

```
yarn styleguide
```

### Build styleguidist

```
yarn styleguide:build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
