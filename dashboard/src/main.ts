import Vue from 'vue';
import VueApollo from 'vue-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import App from './views/App/App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
// import { DashboardServicePlugin } from './plugins';

// HTTP connection to the API
const httpLink = createHttpLink({
    // You should use an absolute URL here
    uri: 'http://localhost:3000/graphql',
});

// Cache implementation
const cache = new InMemoryCache();

// Create the apollo client
const apolloClient = new ApolloClient({
    link: httpLink,
    cache,
});

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
});

// const dashboardService = new DashboardService(apolloProvider);

Vue.use(VueApollo);
// Vue.use(DashboardServicePlugin, { apolloProvider });

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    // inject apolloProvider here like vue-router or vuex
    apolloProvider,
    render: (h) => h(App),
}).$mount('#app');
