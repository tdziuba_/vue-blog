import Vue from 'vue';
import Router, { RawLocation, Route } from 'vue-router';
import Home from './views/Home.vue';
import { getSessionAuthData, isSessionExpired } from '@/helpers/session';
import { TSessionAuthData } from '@/types/SessionAuthData';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '*',
            name: 'NotFound',
            component: () => import(/* webpackChunkName: "notFound" */ './views/NotFound/NotFound.vue'),
        },
        {
            path: '/',
            name: 'dashboard',
            component: Home,
            meta: {
                requiresAuth: true,
            },
        },
        {
            path: '/login',
            name: 'login',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "login" */ './views/Login/Login.vue'),
        },
        {
            path: '/articles',
            name: 'articles',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "articles" */ './views/Articles/Articles.vue'),
            meta: {
                requiresAuth: true,
            },
        },
        {
            path: '/categories',
            name: 'categories',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "categories" */ './views/Categories/Categories.vue'),
            meta: {
                requiresAuth: true,
            },
        },
        {
            path: '/tags',
            name: 'tags',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "categories" */ './views/Tags/Tags.vue'),
            meta: {
                requiresAuth: true,
            },
        },
    ],
});

router.beforeEach((to: Route, from: Route, next: (to?: RawLocation | false | ((vm: Vue) => any) | void) => void) => {
    const sessionToken: TSessionAuthData | undefined = getSessionAuthData();
    if (sessionToken && isSessionExpired()) {
        sessionStorage.clear();
        next({
            path: '/login',
        });
    }

    if (from.name === to.name) {
        return;
    }

    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (!sessionToken) {
            next({
                path: '/login',
            });
        }
        next();
    }
    next();
});

export default router;
