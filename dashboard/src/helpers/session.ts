import { TSessionAuthData, TSessionCurrentUser } from '@/types/SessionAuthData';
import { SESSSION_TOKEN, LOGGED_IN_USER } from '@/constants';

export const getSessionAuthData = (): TSessionAuthData | undefined => {
    const sessionData = sessionStorage.getItem(SESSSION_TOKEN);

    if (!sessionData) {
        return undefined;
    }

    return sessionData ? JSON.parse(sessionData) : undefined;
};

export const isSessionExpired = (): boolean => {
    try {
        const sessionData = getSessionAuthData();

        if (!sessionData || !sessionData.expiresAt) {
            return true; // we do not have any information about authentication expiration - is expired by default
        }

        const expirationTime = new Date(sessionData.expiresAt).getTime();

        return expirationTime <= new Date().getTime();
    } catch (_e) {
        return true;
    }
};

export const setSessionAuthData = (user: TSessionCurrentUser, token: string): void => {
    const expiresAt = new Date().getTime() + 60 * 60 * 1000; // expires after one hour
    sessionStorage.setItem(SESSSION_TOKEN, JSON.stringify({ token, secured: true, expiresAt: new Date(expiresAt) }));
    sessionStorage.setItem(LOGGED_IN_USER, JSON.stringify(user));
};

export const getCurrentUser = () => {};
