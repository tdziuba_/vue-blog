import { Vue as _Vue } from 'vue/types/vue';
import { PluginObject, PluginFunction } from 'vue/types/plugin';
import { VueApollo } from 'vue-apollo/types/vue-apollo';

export default class DashboardService {
    constructor(private apolloService: VueApollo) {}
    public refreshToken() {
        console.log('refreshToken', this);
    }
}
