import { Component, Vue } from 'vue-property-decorator';
import gql from 'graphql-tag';
import { SESSSION_TOKEN, LOGGED_IN_USER } from '@/constants';
import { setSessionAuthData } from '@/helpers/session';

type LoginData = {
    email: string;
    password: string;
    error?: string;
    isLoading?: boolean;
};

const LoginUserInput = gql`
    type LoginUserInput {
        email: String!
        password: String!
    }
`;

@Component
export default class Login extends Vue {
    public data(): LoginData {
        return {
            email: '',
            password: '',
            error: '',
            isLoading: false,
        };
    }

    public async login() {
        this.$data.login = true;
        const { email, password }: Partial<LoginData> = this.$data;
        await this.$apollo
            .query({
                // Query
                query: gql`
                    query Login($user: LoginUserInput!) {
                        login(user: $user) {
                            user {
                                username
                                permissions
                                enabled
                            }
                            token
                        }
                    }
                `,
                // Parameters
                variables: {
                    user: { email, password },
                },
            })
            .catch((e) => {
                this.$data.error = e.errorMessage;
            })
            .then(({ data }: any) => {
                const { user, token } = data!.login;
                setSessionAuthData(user, token);
                this.$data.isLoading = false;
                this.$router.push('/');
            });
    }
}
