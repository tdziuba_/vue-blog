import { Vue, Component } from 'vue-property-decorator';
import Menu from '@/components/Menu/Menu';

@Component({
    components: {
        Menu,
    },
})
export default class NotFound extends Vue {}
