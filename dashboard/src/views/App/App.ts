import { Component, Vue } from 'vue-property-decorator';
import { mapState, mapMutations } from 'vuex';
// import Menu from '@/components/menu/Menu.vue';

@Component({
    components: {
        // Menu,
    },
    // Vuex's component binding helper can use here
    computed: mapState(['count']),
    methods: mapMutations(['increment']),
})
export default class App extends Vue {
    public data() {
        return {};
    }
}
