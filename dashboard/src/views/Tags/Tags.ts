import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import Menu from '@/components/Menu/Menu';
import { TagList, TagForm } from '@/components/Tags';

@Component({
    components: {
        Menu,
        TagList,
        TagForm,
    },
})
export default class Tags extends Vue {}
