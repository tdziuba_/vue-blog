import { Vue as _Vue } from 'vue/types/vue';
import { PluginObject, PluginFunction } from 'vue/types/plugin';
import { VueApollo } from 'vue-apollo/types/vue-apollo';
import { DashboardService } from '../services';

type IOptions = {
    apolloProvider: VueApollo;
};

const DashboardServicePlugin: PluginObject<IOptions | undefined> = {
    /* tslint:disable */
    install(Vue: typeof _Vue, options?: IOptions) {
        if (!options || !options.apolloProvider) {
            return;
        }

        const $service = new DashboardService(options.apolloProvider);
        // tslint:disable-next-line
        Vue.prototype.$service = $service;

        Vue.mixin({
            created() {
                console.log('created:', this);
            },
        });

        return Vue;
    },
};

export default DashboardServicePlugin;
