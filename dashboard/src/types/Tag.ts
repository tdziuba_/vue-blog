export interface ITag {
    label: string;
    name: string;
    _id: string;
}
