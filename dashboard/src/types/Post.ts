import { TCategory } from './Category';

export type TPost = {
    _id: string;
    name: string;
    label: string;
    text: string;
    categories: TCategory[];
};

export type TPostModel = TPost & {
    selected: boolean;
};
