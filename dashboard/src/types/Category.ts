import { TPost } from './Post';

export type TCategory = {
    name: string;
    label: string;
    _id: string;
    posts: TPost[];
};

export type TCategoryModel = TCategory & {
    selected: boolean;
};
