export type TSelectOption = {
    _id?: string | number;
    name: string;
    label: string;
    selected?: boolean;
};

export type TSelect = {};
