export type TSessionAuthData = {
    token: string;
    expiresAt: string;
};

export type TSessionCurrentUser = {
    email: string;
    permissions: string[];
};
