### Examples

with tag: {
\_id: string;
label: string;
name: string;
}

```jsx
const tagItem = {label: "Tag text", _id: 1, name: 'this_is_name'}
<tag :tag="tagItem" />
```

with onTagEdit method:

```jsx
const tagItem = {label: "Tag text", _id: 1, name: 'this_is_name'};
const action = () => alert('edit');
<tag :tag="tagItem" :onTagEdit="action" />
```

with onTagRemove method:

```jsx
const tagItem = {label: "Tag text", _id: 1, name: 'this_is_name'};
const action = () => alert('remove');
<tag :tag="tagItem" :onTagRemove="action" />
```
