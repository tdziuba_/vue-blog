import { Vue, Component, Prop, Emit } from 'vue-property-decorator';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faTag, faTimes } from '@fortawesome/free-solid-svg-icons';
import { ITag } from '@/types/Tag';

@Component({
    components: {
        FontAwesomeIcon,
    },
})
export default class Tag extends Vue {
    @Prop({ default: undefined, type: Object }) public tag: ITag | undefined;
    @Prop({ default: undefined, type: Function }) public onTagEdit: Function | undefined;
    @Prop({ default: undefined, type: Function }) public onTagRemove: Function | undefined;

    name = 'tag';

    data() {
        return {
            icon: {
                faTag,
                faTimes,
            },
            isHidden: false,
        };
    }

    handleTagEdit() {
        this.$props.onTagEdit();
    }

    @Emit()
    handleTagRemove() {
        this.$data.isHidden = true;
        this.$props.onTagRemove();
    }
}
