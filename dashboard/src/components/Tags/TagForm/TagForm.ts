import { Vue, Component, Prop, Emit } from 'vue-property-decorator';
import { ITag } from '@/types/Tag';
import { TFormError } from '@/types/FormError';
import { TCategoryModel, TPostModel } from '@/types';
import { TextInput, SelectField } from '@/components/Form';

@Component({
    components: {
        TextInput,
        SelectField,
    },
})
export default class TagForm extends Vue {
    @Prop()
    tag?: ITag;

    @Prop()
    categories: TCategoryModel[] | undefined;

    @Prop()
    posts: TPostModel[] | undefined;

    @Prop({ default: () => {}, type: Function })
    onSubmit?: Function;

    @Prop({ default: () => {}, type: Function })
    onError?: Function;

    title: string = 'Edit tag';
    name = 'tag-form';

    public data() {
        return {
            selectedCategories: [...this.$props.categories].filter((c) => c.selected),
            selectedPosts: [...this.$props.posts].filter((p) => p.selected),
            label: this.$props.tag?.label || '',
            name: this.$props.tag?.name || '',
            errors: [],
        };
    }

    @Emit()
    handleSubmit() {
        this.validateForm();

        if (this.$data.errors.length) {
            return this.handleError();
        }

        this.$props.onSubmit({ model: this.$data });
    }

    @Emit()
    handleError() {
        const { errors, ...model } = this.$data;
        this.$props.onError({ model, errors });
    }

    @Emit()
    handleChange() {
        this.validateForm();

        if (this.$data.errors.length) {
            return this.handleError();
        }
    }

    validateForm() {
        if (!this.$data.label || this.$data.label.length <= 0 || this.$data.label.length > 85) {
            this.setError({ field: 'label', errorMessage: 'Label must be a string 1-85 characters long' });
        }
    }

    setError(payload: TFormError) {
        this.$data.errors.push(payload);
    }

    getFieldError(field: string) {
        const error = this.$data.errors.find((e: TFormError) => e.field === field);
        return error || undefined;
    }
}
