### Examples

without model

```jsx
const categories = [1,2,3,4,5,6].map(i => ({_id: i + 1, name: `category_${i}`, label: `category #${i+1}`, selected: i % 2 === 0 }));
const posts = [1,2,3,4,5,6].map(i => ({_id: i + 1, name: `post_${i}`, label: `Post #${i+1}`, selected: i % 2 === 0 }));
const onSubmit = () => alert('submitted')
const onError = ({model, errors}) => alert('errors occurred:', errors.map(e => e.errorMessage).join(",\n"))

<tag-form :categories="categories" :posts="posts" :onSubmit="onSubmit" :onError="onError" />
```

with model

```jsx
const categories = [1,2,3,4,5,6].map(i => ({_id: i + 1, name: `category_${i}`, label: `category #${i+1}`, selected: i % 2 === 0 }));
const posts = [1,2,3,4,5,6].map(i => ({_id: i + 1, name: `post_${i}`, label: `Post #${i+1}`, selected: i % 2 === 0 }));
const tag = {label: 'tag test label', _id: '123abc', name: 'tag-test-label'};
const onSubmit = () => alert('submitted')
const onError = ({model, errors}) => alert('errors occurred:', errors.map(e => e.errorMessage).join(",\n"))

<tag-form :categories="categories" :posts="posts" :tag="tag" :onSubmit="onSubmit" :onError="onError" />
```
