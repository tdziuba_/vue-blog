import { Component, Vue } from 'vue-property-decorator';
import gql from 'graphql-tag';
import Tag from '@/components/Tags/Tag/Tag';
import PageTitle from '@/components/PageTitle/PageTitle';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faTags, faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { ApolloQueryResult } from 'apollo-client/core/types';
import Loader from '@/components/Loader/Loader';

@Component({
    components: {
        FontAwesomeIcon,
        Tag,
        PageTitle,
        Loader,
    },
})
export default class TagList extends Vue {
    data() {
        return {
            tags: [],
            icon: {
                faTags,
                faSyncAlt,
            },
            title: 'Tag List',
            isLoading: true,
        };
    }

    created() {
        console.log(this);
    }

    mounted() {
        this.getTags();
    }

    private async getTags() {
        this.$data.isLoading = true;
        await this.$apollo
            .query({
                query: gql`
                    query Tags {
                        getTags {
                            _id
                            name
                            label
                        }
                    }
                `,
            })
            .catch((err: Error) => {
                console.log(err);
                this.$data.isLoading = false;
            })
            .then(({ data }: any) => {
                console.log(data);
                this.$data.isLoading = false;
                this.$data.tags = data.getTags;
            });
    }
}
