import { Component, Prop, Vue } from 'vue-property-decorator';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
    components: {
        FontAwesomeIcon,
    },
})
export default class Loader extends Vue {
    @Prop({ default: 'medium' })
    readonly size!: 'small' | 'medium' | 'big';

    @Prop({ default: faSpinner })
    icon!: IconDefinition;
}
