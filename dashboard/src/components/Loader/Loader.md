Customizable loader

### Examples

Small loader:

```jsx
<loader size="small" />
```

Medium loader:

```jsx
<loader size="medium" />
```

Big loader:

```jsx
<loader size="big" />
```

Custom loader with icon:

```jsx
<loader size="medium" icon="" />
```
