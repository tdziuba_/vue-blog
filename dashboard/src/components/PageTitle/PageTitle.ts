import { Component, Vue, Prop } from 'vue-property-decorator';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

@Component({
    components: {
        FontAwesomeIcon,
    },
})
export default class PageTitle extends Vue {
    @Prop()
    icon: SVGAElement | undefined;

    @Prop(String)
    title: string | undefined;
}
