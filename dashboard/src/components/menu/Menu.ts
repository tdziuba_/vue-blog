import Vue from 'vue';
import Component from 'vue-class-component';
import { SESSSION_TOKEN } from '@/constants';
import gql from 'graphql-tag';

type MenuItem = {
    name: string;
    path: string;
    text: string;
    isActive: boolean;
};

@Component
export default class Menu extends Vue {
    public data() {
        return {
            menu: [
                { name: 'dashboard', path: '/', text: 'Start', isActive: true },
                { name: 'categories', path: '/categories', text: 'Categories', isActive: false },
                { name: 'articles', path: '/articles', text: 'Articles', isActive: false },
                { name: 'tags', path: '/tags', text: 'Tags', isActive: false },
            ],
            active: this.$router.currentRoute.path,
        };
    }

    mounted() {
        this.updateMenu();
    }

    onMenuChange(path: string) {
        console.log(path);
        this.$data.active = path;
        this.updateMenu();
    }

    updateMenu() {
        this.$data.menu = this.$data.menu.map((item: MenuItem) => {
            const isActive = item.path === this.$data.active;
            return { ...item, isActive };
        });
    }

    async logout() {
        sessionStorage.clear();

        await this.$apollo.mutate({
            mutation: gql`
                mutation Logout {
                    logout
                }
            `,
        });

        this.$router.push('/');
    }
}
