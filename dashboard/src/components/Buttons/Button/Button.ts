import { Component, Vue, Prop, Emit, PropSync } from 'vue-property-decorator';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
    components: {
        FontAwesomeIcon,
    },
})
export default class Button extends Vue {
    name: String = 'button';

    @Prop({ default: 'button', type: String })
    type?: 'submit' | 'button' | 'reset';

    @Prop({ default: '', type: String })
    size?: string;

    @Prop({ default: false, type: Boolean })
    expanded?: boolean;

    @PropSync('isLoading', { type: Boolean })
    shouldShowLoader: boolean = false;

    @Prop()
    onClick?: Function;

    public data() {
        return {
            icon: faSpinner,
        };
    }

    private errorCaptured(err: Error, vm: Vue, info: any) {
        console.log(err, vm, info);
        // err: error trace
        // vm: component in which error occured
        // info: Vue specific error information such as lifecycle hooks, events etc.
        // TODO: Perform any custom logic or log to server
        // return false to stop the propagation of errors further to parent or global error handler
        return false;
    }

    getButtonClasses() {
        return {
            ...this.getSizeClass(),
            expanded: this.$props.expanded,
        };
    }

    getSizeClass() {
        return {
            tiny: this.$props.size === 'tiny',
            small: this.$props.size === 'small',
            large: this.$props.size === 'large',
        };
    }

    @Emit()
    handleClick() {
        this.shouldShowLoader = true;
        if (this.$props.onClick) {
            this.$props.onClick();
        }
    }
}
