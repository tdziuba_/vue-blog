import { Vue, Component, Prop, PropSync, Watch, Emit } from 'vue-property-decorator';

type AlertClassNames = {
    hidden: boolean;
    info: boolean;
    warning: boolean;
    error: boolean;
    success: boolean;
};

type AlertType = 'info' | 'warning' | 'error' | 'success';

@Component
export default class Alert extends Vue {
    @PropSync('closable', { type: Boolean }) shouldCloseAfterDelay!: boolean;
    @PropSync('type', { type: String }) alertType!: AlertType;

    name = 'alert';

    @Watch('isHidden')
    onVisibilityChange(isHidden: boolean, prevIsHidden: boolean) {
        if (isHidden !== prevIsHidden) {
            this.$data.isHidden = isHidden;
        }
    }

    public data() {
        return {
            isFocused: false,
            isHidden: false,
        };
    }

    mounted() {
        if (this.shouldCloseAfterDelay) {
            this.startClosing();
        }
    }

    getClassObject(): AlertClassNames {
        return {
            hidden: this.$data.isHidden,
            info: this.alertType === 'info',
            warning: this.alertType === 'warning',
            error: this.alertType === 'error',
            success: this.alertType === 'success',
        };
    }

    startClosing() {
        setTimeout(() => {
            if (this.$data.isFocused) {
                return this.startClosing();
            }

            return this.handleClose();
        }, 5000);
    }

    @Emit()
    handleClose() {
        this.$data.isHidden = true;
    }

    handleMouseOver() {
        this.$data.isFocused = true;
    }

    handleMouseLeave() {
        this.$data.isFocused = false;
    }
}
