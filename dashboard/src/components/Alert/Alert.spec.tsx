import { Vue } from 'vue-property-decorator';
import { mount } from '@vue/test-utils';
import Alert from './Alert.vue';

jest.useFakeTimers();

describe('Alert', () => {
    type AlertProps = Record<string, any>;
    const setup = (propsOverrides: AlertProps = {}, slotsOverrides = {}) => {
        const props = {
            type: 'info',
            ...propsOverrides,
        };
        const slots = {
            default: 'text for alert',
            ...slotsOverrides,
        };
        const wrapper = mount(Alert, {
            propsData: props,
            slots,
        });
        return {
            wrapper,
            alert: wrapper.find('.alert'),
            closeText: '×',
        };
    };

    it('should mount with default options', () => {
        const { wrapper, closeText, alert } = setup();
        expect(wrapper.exists()).toBe(true);
        expect(alert.text()).toBe(`text for alert ${closeText}`);
        expect(wrapper.props()).toEqual({
            closable: false,
            type: 'info',
        });
        wrapper.destroy();
    });

    it('should mount with given message text', () => {
        const { wrapper, alert } = setup({}, { default: 'this is test message' });
        expect(alert.text()).toContain('this is test message');
        wrapper.destroy();
    });

    test.each(['info', 'warning', 'error', 'success'])('should mount with %s class name', (classname) => {
        const { wrapper } = setup({ type: classname });
        expect(wrapper.find(`.${classname}`).exists()).toBe(true);
        wrapper.destroy();
    });

    it('should hide on close click', async (done) => {
        const { wrapper } = setup();
        expect(wrapper.find('.hidden').exists()).toBe(false);
        wrapper.find('.close').trigger('click');
        await Vue.nextTick();
        expect(wrapper.find('.hidden').exists()).toBe(true);
        done();
    });

    it('should hide after delay', async (done) => {
        const { wrapper } = setup({ closable: true });
        expect(wrapper.find('.hidden').exists()).toBe(false);
        expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 5000);

        jest.runAllTimers();
        await Vue.nextTick();
        expect(wrapper.find('.hidden').exists()).toBe(true);
        done();
    });
});
