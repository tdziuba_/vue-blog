### Examples

closable: true

```jsx
<div style="position:relative;">
    <alert type="info" closable>
        Test message
    </alert>
</div>
```

closable: false

```jsx
<div style="position:relative;">
    <alert type="info">Test message</alert>
</div>
```

type: info

```jsx
<div style="position:relative;">
    <alert type="info">Test message</alert>
</div>
```

type: error

```jsx
<div style="position:relative;">
    <alert type="error">Test message</alert>
</div>
```

type: warning

```jsx
<div style="position:relative;">
    <alert type="warning">Test message</alert>
</div>
```

type: success

```jsx
<div style="position:relative;">
    <alert type="success">Test message</alert>
</div>
```
