### Examples

default

```jsx
const onChange = (value) => alert(value)

<text-input :inputName="testField" :onChange="onChange" label="Input label" />
```

with error

```jsx
const onChange = (value) => alert(value);
const error = {field: 'testField', errorMessage: 'This field is required'};

<text-input :inputName="testField" label="Input label" :onChange="onChange" :error="error" />
```
