import { Vue, Component, Prop } from 'vue-property-decorator';
import { TFormError } from '@/types';
import { debounce } from 'lodash';
import { FORM_DEFAULT_DELAY } from '@/constants/delays';

@Component
export default class TextInput extends Vue {
    name = 'text-input';

    @Prop()
    inputName?: string;

    @Prop()
    label?: string;

    @Prop()
    placeholder?: string;

    @Prop()
    required?: boolean;

    @Prop()
    readonly?: boolean;

    @Prop()
    error?: TFormError;

    @Prop({ default: '', type: String })
    model?: string;

    @Prop()
    onChange?: (value: string) => void;

    created() {
        this.handleChange = debounce(this.handleChange, FORM_DEFAULT_DELAY);
    }

    public data() {
        return {
            value: this.$props.model || '',
        };
    }

    handleChange() {
        if (this.$props.onChange) {
            this.$props.onChange(this.$data.value);
        }
    }
}
