default

```jsx
const options = [1,2,3,4,5,6].map(i => ({_id: i + 1, name: `category_${i}`, label: `category #${i+1}`, selected: i % 2 === 0 }));
const inputName = 'categories';
const label = 'Select label';
const onChange = (options) => alert(options.map(o => o.label).join(', '))

<select-field :options="options" :label="label" :inputName="inputName" :onChange="onChange" />
```

with error

```jsx
const options = [1,2,3,4,5,6].map(i => ({_id: i + 1, name: `category_${i}`, label: `category #${i+1}`, selected: i % 2 === 0 }));
const inputName = 'categories';
const label = 'Select label';
const onChange = (options) => alert(options.map(o => o.label).join(', '))
const error = {field: 'categories', errorMessage: 'This is example of error message' }
<select-field :options="options" :label="label" :inputName="inputName" :error="error" />
```
