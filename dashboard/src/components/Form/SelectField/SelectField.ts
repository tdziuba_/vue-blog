import { Vue, Component, Prop } from 'vue-property-decorator';
import { TFormError } from '@/types';
import { TSelectOption } from '@/types';

@Component
export default class SelectField extends Vue {
    name = 'select-field';

    @Prop()
    inputName?: string;

    @Prop()
    label?: string;

    @Prop()
    options?: string;

    @Prop()
    placeholder?: string;

    @Prop()
    required?: boolean;

    @Prop()
    readonly?: boolean;

    @Prop({ default: false, type: Boolean })
    multiple?: boolean;

    @Prop()
    error?: TFormError;

    @Prop({ default: [], type: Array })
    model?: TSelectOption[];

    @Prop()
    onChange?: (options: TSelectOption[]) => void;

    public data() {
        return {
            selectedOptions: this.$props.model,
        };
    }

    handleChange() {
        if (this.$props.onChange) {
            this.$props.onChange(this.$data.selectedOptions);
        }
    }
}
