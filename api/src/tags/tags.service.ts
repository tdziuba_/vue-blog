import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { Connection, Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Tag, ObjectId } from '../graphql.schema';
import { TagDocument, TagSchema } from './schemas/tags.schema';
import { CreateTagDto, UpdateTagDto } from './dto';

@Injectable()
export class TagsService {
  private readonly tags: Tag[];

  constructor(
    @InjectModel('Tag')
    private readonly model: Model<TagDocument>,
  ) {}

  public getAllTags(): Promise<Tag[]> {
    return this.model.find().exec();
  }

  public findOne(name: string): Promise<Tag | null> {
    return this.model.findOne({ name }).exec();
  }

  public findById(_id: ObjectId): Promise<Tag | null> {
    return this.model.findById(_id).exec();
  }

  async create(createTagInput: CreateTagDto): Promise<Tag> {
    const createdTag = new this.model(createTagInput);
    return createdTag.save();
  }

  async update(updateTagInput: UpdateTagDto): Promise<Tag> {
    const updatedTag = await this.model.findOneAndUpdate(
      { name: updateTagInput.name },
      updateTagInput,
      (err, doc) => {
        if (err) {
          throw new HttpException(
            err.message,
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }
        return doc;
      },
    );
    return updatedTag;
  }

  async delete(_id: ObjectId): Promise<Tag> {
    const removedTag = await this.model.findOneAndDelete(
      { _id },
      (err, doc) => {
        if (err) {
          throw new HttpException(
            err.message,
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }
        return doc;
      },
    );

    return removedTag;
  }
}
