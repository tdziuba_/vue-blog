import { ParseIntPipe, ParseUUIDPipe, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';
import { Tag, ObjectId } from '../graphql.schema';
import { TagsGuard } from './tags.guard';
import { TagsService } from './tags.service';
import { CreateTagDto, UpdateTagDto } from './dto';
import { ObjectIdScalar } from 'src/common/scalars/object-id.scalar';

const pubSub = new PubSub();

@Resolver('Tag')
export class TagsResolver {
  constructor(private readonly tagsService: TagsService) {}

  @Query()
  @UseGuards(TagsGuard)
  async getTags() {
    return this.tagsService.getAllTags();
  }

  @Query('tagByName')
  async findOneByName(
    @Args('name')
    name: string,
  ): Promise<Tag> {
    return this.tagsService.findOne(name);
  }

  @Query('tagById')
  async findOneById(
    @Args('_id')
    _id: ObjectId,
  ): Promise<Tag> {
    return this.tagsService.findById(_id);
  }

  @Mutation('createTag')
  async create(@Args('createTagInput') args: CreateTagDto): Promise<Tag> {
    const createdTag = await this.tagsService.create(args);
    pubSub.publish('tagCreated', { tagCreated: createdTag });
    return createdTag;
  }

  @Mutation('updateTag')
  async update(@Args('updateTagInput') args: UpdateTagDto): Promise<Tag> {
    const updatedTag = await this.tagsService.update(args);
    pubSub.publish('tagUpdated', { tagUpdated: updatedTag });
    return updatedTag;
  }

  @Mutation('removeTag')
  async delete(@Args('_id') _id: ObjectId): Promise<Tag> {
    const deletedTag = await this.tagsService.delete(_id);
    pubSub.publish('tagRemoved', { tagRemoved: deletedTag });
    return deletedTag;
  }

  @Subscription('tagCreated')
  tagCreated() {
    return pubSub.asyncIterator('tagCreated');
  }

  @Subscription('tagUpdated')
  tagUpdated() {
    return pubSub.asyncIterator('tagUpdated');
  }

  @Subscription('tagRemoved')
  tagDeleted() {
    return pubSub.asyncIterator('tagRemoved');
  }
}
