import { Controller, Get, Post, Patch, Body, Param } from '@nestjs/common';
import { TagsService } from './tags.service';
import { CreateTagDto, UpdateTagDto } from './dto';
import { getSeoName } from '../helpers';

@Controller('tags')
export class TagsController {
    public constructor(private service: TagsService) {}

    @Get()
    public async getAllTags() {
        return await this.service.getAllTags();
    }

    @Get(':name')
    async findOne(@Param('name') name: string) {
        return await this.service.findOne(getSeoName(name));
    }

    @Post()
    public async createTag(@Body() createTagDto: CreateTagDto) {
        return await this.service.create({ ...createTagDto, name: getSeoName(createTagDto.name) });
    }

    @Patch()
    public async updateTag(@Body() updateTagDto: UpdateTagDto) {
        return await this.service.update(updateTagDto);
    }
}
