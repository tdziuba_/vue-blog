import { MaxLength, Min, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ObjectType, Field, Int } from 'type-graphql';
import { UpdateTagInput } from '../../graphql.schema';

@ObjectType()
export class UpdateTagDto extends UpdateTagInput {
  @Field()
  @IsNotEmpty()
  @MaxLength(85)
  @ApiProperty()
  name: string;

  @Field()
  @IsNotEmpty()
  @MaxLength(85)
  @ApiProperty()
  label: string;

  // @Field(() => UpdatePostInput)
  // posts: [UpdatePostInput];
}
