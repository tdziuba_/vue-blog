import { MaxLength, Min, IsNotEmpty, IsNumber } from 'class-validator';
import { ObjectType, Field, Int } from 'type-graphql';
import { ApiProperty } from '@nestjs/swagger';
import { CreateTagInput } from '../../graphql.schema';

@ObjectType()
export class CreateTagDto extends CreateTagInput {
  // @Field(() => Int)
  // @IsNumber()
  // @Min(1)
  // readonly _id?: number;

  @Field()
  @IsNotEmpty()
  @MaxLength(85)
  @ApiProperty()
  name: string;

  @Field()
  @IsNotEmpty()
  @MaxLength(85)
  @ApiProperty()
  label: string;

  // @Field(() => CreatePostInput)
  // @ApiProperty()
  // posts?: [CreatePostInput];
}
