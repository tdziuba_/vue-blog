import { Schema, model, Model, Document, Query, Types } from 'mongoose';
import { Tag } from '../../graphql.schema';
// import { PostSchema } from '../../posts/schema/posts.schema';

export interface TagDocument extends Tag, Document {
  // Declaring everything that is not in the GraphQL Schema for a Tag
}

export const TagSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
      required: true,
    },
    label: {
      type: String,
      unique: true,
      required: true,
    },
    // posts: [PostSchema],
  },
  {
    versionKey: false,
  },
);
