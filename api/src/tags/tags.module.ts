import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TagsController } from './tags.controller';
import { TagsService } from './tags.service';
import { TagsResolver } from './tags.resolver';
import { TagSchema } from './schemas/tags.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Tag', schema: TagSchema }])],
  controllers: [TagsController],
  providers: [TagsService, TagsResolver],
})
export class TagsModule {}
