import { TagsModule } from './tags/tags.module';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users';

/**
 * check if app is running in production environment
 * @returns boolean
 */
const isProduction = () => process.env.ENV === 'production';

@Module({
  imports: [
    TagsModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    UsersModule,
    GraphQLModule.forRoot({
      typePaths: ['./**/*.graphql'],
      installSubscriptionHandlers: true,
      playground: !isProduction(),
      context: ({ req, res }: any) => ({ req, res }),
    }),
    MongooseModule.forRoot(
      `mongodb://${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}/blog`,
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
