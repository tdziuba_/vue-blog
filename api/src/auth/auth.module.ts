import { Module, forwardRef } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { UsersModule } from '../users/users.module';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthResolver } from './auth.resolvers';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt', session: false }),
    JwtModule.registerAsync({
      imports: [],
      useFactory: () => {
        const options: JwtModuleOptions = {
          secret: process.env.JWT_SECRET,
          signOptions: {
            expiresIn: process.env.jwtExpiresIn || 1800,
          },
        };
        return options;
      },
      inject: [],
    }),
    forwardRef(() => UsersModule),
  ],
  providers: [AuthService, AuthResolver, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
