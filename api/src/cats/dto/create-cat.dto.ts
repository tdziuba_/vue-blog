import { Min, MinLength, MaxLength } from 'class-validator';
import { CreateCatInput } from '../../graphql.schema';

export class CreateCatDto extends CreateCatInput {
  @Min(1)
  age: number;

  @MinLength(1)
  @MaxLength(255)
  name: string;

  @MinLength(1)
  @MaxLength(255)
  breed: string;
}
