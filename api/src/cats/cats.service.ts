import { Injectable } from '@nestjs/common';
// import { Cat } from '../graphql.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCatDto } from './dto/create-cat.dto';
import { Cat } from './schemas/cat.schema';

@Injectable()
export class CatsService {
  // private readonly cats: Cat[] = [{ id: 1, name: 'Cat', age: 5 }];

  constructor(@InjectModel(Cat.name) private readonly catModel: Model<Cat>) {}

  async create(createCatDto: CreateCatDto): Promise<Cat> {
    const createdCat = new this.catModel(createCatDto);
    return createdCat.save();
  }

  async findAll(): Promise<Cat[]> {
    return this.catModel.find().exec();
  }

  findOneByName(name: string): Promise<Cat> {
    return this.catModel.findOne({ name }, (err, res) => res).exec();
  }
}
