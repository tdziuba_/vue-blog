import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import * as session from 'express-session';
import { AppModule } from './app.module';

dotenv.config();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const swaggerOptions = new DocumentBuilder()
    .setTitle('Blog API')
    .setDescription('The blog API description')
    .setVersion('1.0')
    .addTag('blog')
    .build();
  const document = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api', app, document);

  app.use(helmet());
  app.enableCors();
  app.use(bodyParser());

  app.use((_req, res, next) => {
    res.removeHeader('X-Powered-By');
    res.setHeader('X-Powered-By', 'ApiGenerator 2.0.2');

    next();
  });

  app.use(
    session({
      name: process.env.SESSION_NAME,
      secret: process.env.SESSION_SECRET,
      saveUninitialized: false,
      resave: false,
      cookie: {
        secure: process.env.env === 'production',
        maxAge: 900000,
        expires: new Date(Date.now() + 1000 * 60 * 60),
        httpOnly: true,
      },
    }),
  );

  app.use(
    rateLimit({
      windowMs: process.env.env === 'production' ? 900000 : 60000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );

  await app.listen(3000);
}
bootstrap();
