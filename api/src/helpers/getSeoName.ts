import { kebabCase } from 'lodash';

export const getSeoName = (name: string): string => kebabCase(name);
