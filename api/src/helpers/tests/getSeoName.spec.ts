import { getSeoName } from '../';

describe('getSeoName', () => {
    const cases = [
        ['React testing', 'react-testing'],
        ['NestJs controller', 'nest-js-controller'],
        ['Nestjs controller', 'nestjs-controller'],
        ['Zażółć gęślą jaźń', 'zazolc-gesla-jazn'],
        ['string-with-dashes', 'string-with-dashes'],
    ];
    test.each(cases)('given %p as argument, returns %p', (name, expectedResult) => {
        expect(getSeoName(name)).toEqual(expectedResult);
    });
});
