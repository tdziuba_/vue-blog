
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class CreateCatInput {
    name?: string;
    age?: number;
}

export class CreateTagInput {
    name: string;
    label: string;
}

export class CreateUserInput {
    username: string;
    email: string;
    password: string;
}

export class LoginUserInput {
    username?: string;
    email?: string;
    password: string;
}

export class UpdatePasswordInput {
    oldPassword: string;
    newPassword: string;
}

export class UpdateTagInput {
    _id: ObjectId;
    name: string;
    label: string;
}

export class UpdateUserInput {
    username?: string;
    email?: string;
    password?: UpdatePasswordInput;
    enabled?: boolean;
}

export class Cat {
    id?: number;
    name?: string;
    age?: number;
}

export class LoginResult {
    user: User;
    token: string;
}

export class LogoutResult {
    isLoggedOut: boolean;
}

export abstract class IMutation {
    abstract logout(): boolean | Promise<boolean>;

    abstract createCat(createCatInput?: CreateCatInput): Cat | Promise<Cat>;

    abstract createTag(createTagInput?: CreateTagInput): Tag | Promise<Tag>;

    abstract updateTag(updateTagInput?: UpdateTagInput): Tag | Promise<Tag>;

    abstract removeTag(_id: ObjectId): Tag[] | Promise<Tag[]>;

    abstract createUser(createUserInput?: CreateUserInput): User | Promise<User>;

    abstract updateUser(fieldsToUpdate: UpdateUserInput, username?: string): User | Promise<User>;

    abstract addAdminPermission(username: string): User | Promise<User>;

    abstract removeAdminPermission(username: string): User | Promise<User>;

    abstract resetPassword(username: string, code: string, password: string): User | Promise<User>;
}

export abstract class IQuery {
    abstract login(user: LoginUserInput): LoginResult | Promise<LoginResult>;

    abstract refreshToken(): string | Promise<string>;

    abstract getCats(): Cat[] | Promise<Cat[]>;

    abstract cat(id: string): Cat | Promise<Cat>;

    abstract getTags(): Tag[] | Promise<Tag[]>;

    abstract tagById(_id: ObjectId): Tag | Promise<Tag>;

    abstract tagByName(name: string): Tag | Promise<Tag>;

    abstract users(): User[] | Promise<User[]>;

    abstract user(username?: string, email?: string): User | Promise<User>;

    abstract forgotPassword(email?: string): boolean | Promise<boolean>;
}

export abstract class ISubscription {
    abstract catCreated(): Cat | Promise<Cat>;

    abstract tagCreated(): Tag | Promise<Tag>;

    abstract tagUpdated(): Tag | Promise<Tag>;

    abstract tagRemoved(): Tag | Promise<Tag>;
}

export class Tag {
    name: string;
    label: string;
    _id: ObjectId;
}

export class User {
    username: string;
    email: string;
    permissions: string[];
    createdAt: Date;
    updatedAt: Date;
    lastSeenAt: Date;
    enabled: boolean;
    _id: ObjectId;
}

export type ObjectId = any;
