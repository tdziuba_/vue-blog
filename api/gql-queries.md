# Examples of GraphQL queries and mutations

## Users

Create user:

```
mutation CreateUser {
  createUser(createUserInput: {
    username: "Tomasz Dziuba",
    email: "tomasz.dziuba@poczta.pl",
    password: "8znakow!!!"
  }) {
    username,
    lastSeenAt,
    updatedAt,
    permissions
  }
}
```

Login user:

```
query Login {
    login(user: {
        username: "Tomasz Dziuba",
        email: "tomasz.dziuba@poczta.pl",
        password: "8znakow!!!"
    }) {
        user {
          username,
          permissions,
          enabled
        },
        token
    }
}
```

Logout

```
mutation Logout {
  logout
}
```

Add permissions:

```

mutation AddAdminPermissions {
  addAdminPermission(username: "Tomasz Dziuba") {
    username
    lastSeenAt
    updatedAt
    permissions
  }
}

```

## Tags

Get Tags

```
query Tags {
  getTags {
    _id,
    name,
    label
  }
}
```

Find tag by id

```
query TagById($id: ObjectId!) {
  tagById(_id: $id) {
    _id,
    name,
    label
  }
}
```

Find tag by name

```
query TagByName($name: String!) {
  tagByName(name: $name) {
    _id,
    name,
    label
  }
}
```

Create tag

```
mutation CreateTag($createTagInput: CreateTagInput!) {
  createTag(createTagInput: $createTagInput) {
    _id,
    name,
    label
  }
}
```

Update tag
